//
//  ReminderNotificationManager.swift
//  BalansioSmartXcode7
//
//  Created by Topi Penttilä on 25/11/16.
//  Copyright © 2016 iosdev. All rights reserved.
//

import Foundation
import UserNotifications

class ReminderNotificationManager {
    
    static let sharedInstance: ReminderNotificationManager = ReminderNotificationManager()
    
    // Schedules the default notification timing
    // Period can't be nil when this func called
    func scheduleDefaultDisciplineReminderTiming(goal: Goal) {
        let dataType = goal.getDataType()
        let period = goal.getDiscipline()?.getPeriod()
        let amount = goal.getDiscipline()?.getAmount()
        
        if period != nil && amount != nil && dataType != nil {
            switch period! {
            case .Day:
                let calendar: NSCalendar = NSCalendar.currentCalendar()
                var dateFire = NSDate()
                var fireComponents = calendar.components([NSCalendarUnit.Year, NSCalendarUnit.Month, NSCalendarUnit.Day, NSCalendarUnit.Hour, NSCalendarUnit.Minute, NSCalendarUnit.Second], fromDate: dateFire)
                fireComponents.hour = 9
                fireComponents.minute = 0
                dateFire = calendar.dateFromComponents(fireComponents)!
                var incrementTimeInterval = 0.0
                if amount! > 1 {
                    incrementTimeInterval = Double(43200 / (amount!-1))
                    for i in 1...amount!-1 {
                        scheduleNotificationForIndex(dataType!, index: i, fireComponents: fireComponents, dateFire: dateFire)
                        dateFire = dateFire.dateByAddingTimeInterval(incrementTimeInterval)
                        fireComponents = calendar.components([NSCalendarUnit.Hour, NSCalendarUnit.Minute], fromDate: dateFire)
                    }
                    fireComponents.hour = 21
                    scheduleNotificationForIndex(dataType!, index: amount!, fireComponents: fireComponents, dateFire: dateFire)
                }
                
            case .Week:
                let calendar: NSCalendar = NSCalendar.currentCalendar()
                let dateFire = NSDate()
                let fireComponents = calendar.components([NSCalendarUnit.Day, NSCalendarUnit.Hour], fromDate: dateFire)
                if amount == 1 {
                    fireComponents.day = 3
                }
            default:
                print("something")
            }
        }
    }
    
    func scheduleNotificationForIndex(dataType: HealthDataType, index: Int, fireComponents: NSDateComponents, dateFire: NSDate) {
        if #available(iOS 10.0, *) {
            let trigger = UNCalendarNotificationTrigger(dateMatchingComponents: fireComponents, repeats: true)
            
            let notificationContent = UNMutableNotificationContent()
            notificationContent.title = dataType.rawValue + " measurement"
            notificationContent.body = "Time for a " + dataType.rawValue + " measurement number " + String(index) + "."
            notificationContent.categoryIdentifier = dataType.rawValue
            
            let request = UNNotificationRequest(
                identifier: String(index) + dataType.rawValue,
                content: notificationContent,
                trigger: trigger
            )
            UNUserNotificationCenter.currentNotificationCenter().addNotificationRequest(request, withCompletionHandler: nil)
            print("scheduled notification" + String(request))
            
            //Save to Realm
            let notification: Notification = Notification()
            notification.set(request.identifier, dataType: dataType, timestamp: dateFire, notificationType: .Reminder)
            RealmManager.sharedInstance.saveNotificationToRealm(notification)
            
        } else {
            // Fallback on earlier versions
        }
    }
    
    func rescheduleNotification(dataEntry: DataEntry) {
        let dataType = dataEntry.getDataType()
        var index = 0
        GoalManager.sharedInstance.getGoalProgress(dataEntry)  { (progress, amount) in
            index = progress
            print("progress" + String(progress))
        }
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.currentNotificationCenter().removeDeliveredNotificationsWithIdentifiers([String(index) + dataType.rawValue])
            
            let calendar: NSCalendar = NSCalendar.currentCalendar()
            //Schedule Notification starting from tomorrow (1 minute before current time)
            let dateFire = NSDate().dateByAddingTimeInterval(-60)
            let fireComponents = calendar.components([NSCalendarUnit.Hour, NSCalendarUnit.Minute], fromDate: dateFire)
            RealmManager.sharedInstance.deleteNotification(index, dataType: dataType)
            scheduleNotificationForIndex(dataType, index: index, fireComponents: fireComponents, dateFire: dateFire)
            print("rescheduled notification " + String(index) + dataType.rawValue + String(fireComponents))
            
        } else {
            // Fallback on earlier versions
        }
    }
    
    
    func scheduleDemoDefaultReminderTiming(goal: Goal) {
        let dataType = goal.getDataType()
        let period = goal.getDiscipline()?.getPeriod()
        let amount = goal.getDiscipline()?.getAmount()
        
        if period != nil && amount != nil && dataType != nil {
            switch period! {
            case .Day:
                let calendar: NSCalendar = NSCalendar.currentCalendar()
                var dateFire = NSDate().dateByAddingTimeInterval(10)
                var fireComponents = calendar.components([NSCalendarUnit.Year, NSCalendarUnit.Month, NSCalendarUnit.Day, NSCalendarUnit.Hour, NSCalendarUnit.Minute, NSCalendarUnit.Second], fromDate: dateFire)
                dateFire = calendar.dateFromComponents(fireComponents)!
                var incrementTimeInterval = 0.0
                if amount! > 1 {
                    incrementTimeInterval = Double(300 / (amount!-1))
                    for i in 1...amount!-1 {
                        scheduleNotificationForIndex(dataType!, index: i, fireComponents: fireComponents, dateFire: dateFire)
                        dateFire = dateFire.dateByAddingTimeInterval(incrementTimeInterval)
                        fireComponents = calendar.components([NSCalendarUnit.Hour, NSCalendarUnit.Minute, NSCalendarUnit.Second], fromDate: dateFire)
                        
                    }
                }
                
            default:
                print("something")
            }
        }
        
    }
}

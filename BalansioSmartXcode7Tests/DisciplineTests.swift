//
//  DisciplineTests.swift
//  BalansioSmartXcode7
//
//  Created by Binh Tran on 09/11/2016.
//  Copyright © 2016 iosdev. All rights reserved.
//

import XCTest
@testable import BalansioSmartXcode7

class DisciplineTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testNormal() {
        let discipline = Discipline()
        discipline.set(5, period: Period.Day)
        XCTAssert(discipline.getAmount() == 5 && discipline.getPeriod().rawValue == "day")
    }
    
    func testRandom() {
        let discipline = Discipline()
        let amount = Int(arc4random())
        discipline.set(amount, period: Period.Month)
        XCTAssert(discipline.getAmount() == amount && discipline.getPeriod().rawValue == "month")
    }
    
   
    
}

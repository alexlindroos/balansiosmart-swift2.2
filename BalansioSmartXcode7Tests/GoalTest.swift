//
//  GoalTest.swift
//  BalansioSmartXcode7
//
//  Created by Binh Tran on 15/11/2016.
//  Copyright © 2016 iosdev. All rights reserved.
//

import XCTest
@testable import BalansioSmartXcode7

class GoalTest: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testSetEverythingRight() {
        let goal = Goal()
        let clinical = Clinical()
        clinical.set(2.0, max: 3.0, unit: "")
        let discipline = Discipline()
        discipline.set(5, period: Period.periodFromString("day")!)
        let rule = Rule()
        rule.set(NotificationSetting.Easy)
        
        goal.set(HealthDataType.BloodGlucose, discipline: discipline, clinical: clinical, rule: rule, timestamp: NSDate())
        
        XCTAssert(goal.getDataType() == HealthDataType.BloodGlucose)
        XCTAssert(goal.getClinical()?.getMax() == 3 && goal.getClinical()?.getMin() == 2)
        XCTAssert((goal.getDiscipline()?.getAmount())! == 5 && (goal.getDiscipline()?.getPeriod().rawValue)! == "day")
        XCTAssert(goal.getRule()?.get().rawValue == "Easy")
    }
    
}

//
//  NotificationTest.swift
//  BalansioSmartXcode7
//
//  Created by Binh Tran on 14/12/2016.
//  Copyright © 2016 iosdev. All rights reserved.
//

import XCTest
@testable import BalansioSmartXcode7

class NotificationTest: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testNormal() {
        let notification = Notification()
        let time = NSDate()
        notification.set("1", dataType: HealthDataType.BloodGlucose, timestamp: time, notificationType: NotificationType.Reminder)
        // Test set every value correctly
        XCTAssert(notification.getId() == "1")
        XCTAssert(notification.getDataType() == HealthDataType.BloodGlucose)
        XCTAssert(notification.getDate() == time)
        XCTAssert(notification.getNotificationType() == NotificationType.Reminder)
    }
}

//
//  User.swift
//  BalansioSmartXcode7
//
//  Created by Binh Tran on 23/11/2016.
//  Copyright © 2016 iosdev. All rights reserved.
//

import Foundation
import RealmSwift

class User: Object {
    
    dynamic var name: String = "Kauko"
    dynamic var age: Int = 20
    dynamic var height: Double = 175 // cm
    dynamic var weight: Double = 75 // kg
    
    override static func primaryKey() -> String? {
        return "name"
    }

}

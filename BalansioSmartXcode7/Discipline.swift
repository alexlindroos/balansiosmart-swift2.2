//
//  Discipline.swift
//  BalansioSmartXcode7
//
//  Created by Topi Penttilä on 01/11/16.
//  Copyright © 2016 iosdev. All rights reserved.
//

import Foundation
import RealmSwift
import HealthKit

enum Period: String {
    case Day = "day"
    case Week = "week"
    case Month = "month"
    case Year = "year"
    
    static func periodFromString(periodString: String) -> Period? {
        switch periodString {
        case "day":
            return .Day
        case "week":
            return .Week
        case "month":
            return .Month
        case "year":
            return .Year
        default:
            return nil
        }
    }
}

class Discipline: Object {
    
    private dynamic var amount: Int = 0
    private dynamic var rawPeriod = ""
    internal var period: Period {
        get {
            return Period(rawValue: rawPeriod)!
        }
        set {
            rawPeriod = newValue.rawValue
        }
    }
    
    override static func ignoredProperties() -> [String] {
        return ["period"]
    }
    
    
    func set(amount: Int, period: Period) -> Discipline {
        self.amount = amount
        self.period = period
        print(self.period)
        //save()
        return self
    }
    
    func getAmount() -> Int {
        return self.amount
    }

    func getPeriod() -> Period {
        return self.period
    }
    
    func save() {
        // Get the default Realm
        let realm = try! Realm()
        // Add to Realm
        try! realm.write {
            realm.add(self)
            print(self)
        }
    }
}

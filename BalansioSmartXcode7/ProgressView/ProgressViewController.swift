//
//  ProgressViewController.swift
//  BalansioSmartXcode7
//
//  Created by Hai Phan on 11/24/16.
//  Copyright © 2016 iosdev. All rights reserved.
//

import UIKit
import MBCircularProgressBar

class ProgressViewController: UIViewController {
    //MARK: IBOutlets
    @IBOutlet weak var notificationViewController: UIView!
    @IBOutlet weak var goalTableView: UITableView!
    
    //MARK: Properties
    var goalArray = DbUtils.getAllGoals()
    
    //MARL: View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        goalTableView.delegate = self
        goalTableView.dataSource = self
        
        //Observer
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ProgressViewController.refreshGoalTableView), name: "refreshProgressView", object: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func refreshGoalTableView() {
        dispatch_async(dispatch_get_main_queue()) {
            self.goalArray = DbUtils.getAllGoals()
            self.goalTableView.reloadData()
        }
    }
    
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
}

//MARK: UITableViewDataSource
extension ProgressViewController: UITableViewDataSource {
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return goalArray.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("goalCell", forIndexPath: indexPath) as! GoalTableViewCell
        let goal = goalArray[indexPath.row]
        cell.configure(goal)
        return cell
    }
}

//MARK: UITableViewDelegate
extension ProgressViewController: UITableViewDelegate {
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        performSegueWithIdentifier("ProgressViewToDetailedView", sender: indexPath)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "ProgressViewToDetailedView" {
            let detailGoalVC = segue.destinationViewController as! GoalDetailViewController
            if let indexPath = sender as? NSIndexPath {
                let goal = goalArray[indexPath.row]
                detailGoalVC.goal = goal
                
            }
        }
    }
}



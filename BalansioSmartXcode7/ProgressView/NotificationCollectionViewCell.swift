//
//  NotificationCollectionViewCell.swift
//  BalansioSmartXcode7
//
//  Created by Hai Phan on 11/24/16.
//  Copyright © 2016 iosdev. All rights reserved.
//

import UIKit
import UserNotifications

class NotificationCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var motivationCardLabel: UILabel!
    @IBOutlet weak var dataTypeImageView: UIImageView!
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var timestampLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        layer.masksToBounds = false
        motivationCardLabel.text = NSLocalizedString("motivationCard1", comment: "")
    }
    
    @available(iOS 10.0, *)
    func configure(notification: UNNotification) {
        motivationCardLabel.text = notification.request.content.body
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateStyle = .MediumStyle
        dateFormatter.timeStyle = .ShortStyle
        timestampLabel.text = dateFormatter.stringFromDate(notification.date)
        
        guard let dataType = HealthDataType.healthDataTypeFromString(notification.request.content.categoryIdentifier) else {return}
        dataTypeImageView.image = UIImage(named: "\(String(dataType).lowercaseString)_icon")
        backgroundImageView.image = UIImage(named: "\(String(dataType).lowercaseString)_background")
    }
}


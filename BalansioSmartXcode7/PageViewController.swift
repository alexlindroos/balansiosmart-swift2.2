//
//  PageViewController.swift
//  BalansioSmartXcode7
//
//  Created by iosdev on 22.11.2016.
//  Copyright © 2016 iosdev. All rights reserved.
//

import UIKit

class PageViewController: UIPageViewController {
    
    // Hardcoded data for headers,images and descriptions
    var pageHeaders = [NSLocalizedString("pageHeader1", comment: ""), NSLocalizedString("pageHeader2", comment: ""), NSLocalizedString("pageHeader3", comment: ""), NSLocalizedString("pageHeader4", comment: "")]
    var pageImages = ["App2", "App2", "App1", "App1"]
    var pageDescriptions = [NSLocalizedString("pageDescription1", comment: ""), NSLocalizedString("pageDescription2", comment: ""), NSLocalizedString("pageDescription3", comment: ""), ""]
    
    // Hides statusbar
    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        //This class is the PageViewControllers data source
        self.dataSource = self
        
        
        // Creates first walkthrough
        if let startWalkthroughVC = self.viewControllerAtIndex(0) {
            setViewControllers([startWalkthroughVC], direction: .Forward, animated: true, completion: nil)
        }
        
    }
    
    // MARK: - Navigate
    func nextPageWithIndex(index: Int)
    {
        if let nextWalkthroughVC = self.viewControllerAtIndex(index+1) {
            setViewControllers([nextWalkthroughVC], direction: .Forward, animated:
                true, completion: nil)
        }
    }
    
    func viewControllerAtIndex(index: Int) -> WalkthroughViewController? {
        if index == NSNotFound || index < 0 || index >= self.pageDescriptions.count{
            return nil
        }
        if let walkThroughViewController = storyboard?.instantiateViewControllerWithIdentifier("WalkthroughViewController") as? WalkthroughViewController{
            walkThroughViewController.imageName = pageImages[index]
            walkThroughViewController.headerText = pageHeaders[index]
            walkThroughViewController.descriptionText = pageDescriptions[index]
            walkThroughViewController.index = index
            
            return walkThroughViewController
        }
        return nil
    }
}


// MARK: - UIPageViewControllerDataSource

extension PageViewController : UIPageViewControllerDataSource {
    func pageViewController(pageViewController: UIPageViewController, viewControllerBeforeViewController viewController: UIViewController) -> UIViewController? {
        var index = (viewController as! WalkthroughViewController).index
        index--
        return self.viewControllerAtIndex(index)
    }
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerAfterViewController viewController: UIViewController) -> UIViewController? {
        var index = (viewController as! WalkthroughViewController).index
        index++
        return self.viewControllerAtIndex(index)
    }
}

















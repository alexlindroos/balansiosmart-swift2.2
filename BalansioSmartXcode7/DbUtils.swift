//
//  DbUtils.swift
//  BalansioSmartXcode7
//
//  Created by Binh Tran on 17/11/2016.
//  Copyright © 2016 iosdev. All rights reserved.
//

import Foundation
import RealmSwift

class DbUtils {
    
    /**
     * Description: Get all Goal objects from realm
     */
    static func getAllGoals() -> Results<Goal> {
        let objs: Results<Goal> = {
            try! Realm().objects(Goal)
        }()
        
        return objs
    }
    
    /*
     * Method to generate default goals and save them to Realm
     */
    static func generateDefaultGoals () {
        let realm = try! Realm()
        // Array of default data types
        let defaulDataTypes = [HealthDataType.BloodGlucose, HealthDataType.Sleep, HealthDataType.A1c, HealthDataType.Activity, HealthDataType.BloodPressureSystolic, HealthDataType.BloodPressureDiastolic, HealthDataType.Weight]
    
        // Array of default clinicals
        let defaultClinicals = [Clinical(value: ["min": 4.5, "max": 7, "unit": "mmol/l"]),
                               Clinical(value: ["min": 7, "unit": "hour"]),
                               Clinical(value: ["max": 7, "unit": "%"]),
                               Clinical(value: ["min": 2.5, "unit": "hours"]),
                               Clinical(value: ["min": 100, "max": 150]),
                               Clinical(value: ["min": 70, "max": 90]),
                               Clinical(value: ["max": 0.5, "unit": "kg"])]
        // Array of default disciplines
        let defaultDisciplines = [Discipline(value: ["amount": 10, "rawPeriod": Period.Day.rawValue]),
                                  Discipline(value: ["amount": 1, "rawPeriod": Period.Day.rawValue]),
                                  Discipline(value: ["amount": 1, "rawPeriod": Period.Day.rawValue]),
                                  Discipline(value: ["amount": 1, "rawPeriod": Period.Week.rawValue]),
                                  Discipline(value: ["amount": 1, "rawPeriod": Period.Day.rawValue]),
                                  Discipline(value: ["amount": 1, "rawPeriod": Period.Day.rawValue]),
                                  Discipline(value: ["amount": 1, "rawPeriod": Period.Week.rawValue])]
        
        let defaultRules = [Rule(value: ["rawSetting": NotificationSetting.Strict.rawValue]),
                            Rule(value: ["rawSetting": NotificationSetting.Easy.rawValue]),
                            Rule(value: ["rawSetting": NotificationSetting.None.rawValue])]
        
        // Begin the write transaction to write default goals into Realm
        try! realm.write() {
            for type in defaulDataTypes {
                let index = defaulDataTypes.indexOf(type)
                let newGoal = Goal()
                newGoal.set(type, discipline: defaultDisciplines[index!], clinical: defaultClinicals[index!], rule: defaultRules[0], timestamp: NSDate())
                realm.add(newGoal, update: true)
                print("Add default goal, type \(type)")
            }
        }
    }
    
    /**
     * Description: static method to add new user or update the old one
     * Parameters: User object
     */
    static func addOrUpdateSingleUser(user: User) {
        let realm = try! Realm()
        try! realm.write() {
            realm.add(user, update: true)
        }
    }
    
    
    
}

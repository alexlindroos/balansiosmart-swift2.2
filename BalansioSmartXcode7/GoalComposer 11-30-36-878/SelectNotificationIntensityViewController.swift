//
//  SelectNotificationIntensityViewController.swift
//  BalansioSmartXcode7
//
//  Created by Hai Phan on 11/3/16.
//  Copyright © 2016 iosdev. All rights reserved.
//

import UIKit

class SelectNotificationIntensityViewController: UIViewController {
    //MARK: IBOutlets
    @IBOutlet weak var instructionLabel: UILabel!
    @IBOutlet weak var notificationIntensityTableView: UITableView!
    @IBOutlet weak var continueButton: UIButton!
    //MARK: Properties
    weak var goalComposerContainerVC: GoalComposerContainerViewController?
    let notificationIntensityOptions = [NotificationSetting.Strict, NotificationSetting.Easy, NotificationSetting.None]
    
    //MARK: View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        goalComposerContainerVC = parentViewController as? GoalComposerContainerViewController
        
        //UITableView
        notificationIntensityTableView.dataSource = self
        notificationIntensityTableView.tableFooterView = UIView()
        
        if let type = goalComposerContainerVC?.goal.getDataType() {
            instructionLabel.text = NSLocalizedString("notification_intensity_text", comment: "") + (type.rawValue.lowercaseString)
        }
        print(goalComposerContainerVC?.goal)
        if goalComposerContainerVC?.mode == .Edit {
            if let notification = goalComposerContainerVC?.editingGoal?.getRule()?.get() {
                notificationIntensityTableView.selectRowAtIndexPath(NSIndexPath(forRow: notification.hashValue, inSection: 0), animated: true, scrollPosition: .None)
            }
        }
        
        continueButton.setTitle(NSLocalizedString("continue", comment: ""), forState: UIControlState.Normal)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    @IBAction func didTapContinue(sender: UIButton) {
        guard let indexPath = notificationIntensityTableView.indexPathForSelectedRow else {
            let alertVC = UIAlertController(title: nil, message: NSLocalizedString("alert1", comment: ""), preferredStyle: .Alert)
            let cancelAction = UIAlertAction(title: NSLocalizedString("dismiss", comment: ""), style: .Cancel, handler: nil)
            alertVC.addAction(cancelAction)
            presentViewController(alertVC, animated: true, completion: nil)
            return
        }
        //Set rule
        let rule = Rule()
        rule.set(notificationIntensityOptions[indexPath.row])
        goalComposerContainerVC?.goal.set(nil, discipline: nil, clinical: nil, rule: rule, timestamp: nil)
        //Set timestamp
        goalComposerContainerVC?.goal.set(nil, discipline: nil, clinical: nil, rule: nil, timestamp: NSDate())
        performSegueWithIdentifier("FromNotificationIntensityToGoalOverview", sender: nil)
    }
    
    @IBAction func didTapDismiss(sender: UIBarButtonItem) {
        goalComposerContainerVC?.dismissViewControllerAnimated(true, completion: nil)
    }
}

//MARK: UITableViewDataSource
extension SelectNotificationIntensityViewController: UITableViewDataSource {
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notificationIntensityOptions.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("notificationIntensityCell", forIndexPath: indexPath) as! NotificationIntensityTableViewCell
        cell.notificationIntensityLabel.text = notificationIntensityOptions[indexPath.row].rawValue
        return cell
    }
}

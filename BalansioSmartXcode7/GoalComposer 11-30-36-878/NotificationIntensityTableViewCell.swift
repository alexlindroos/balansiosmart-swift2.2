//
//  NotificationIntensityTableViewCell.swift
//  BalansioSmartXcode7
//
//  Created by Hai Phan on 11/3/16.
//  Copyright © 2016 iosdev. All rights reserved.
//

import UIKit

class NotificationIntensityTableViewCell: UITableViewCell {

    @IBOutlet weak var checkBoxImageView: UIImageView!
    @IBOutlet weak var notificationIntensityLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        if selected {
            checkBoxImageView.image = UIImage(named: "checked_icon")
        } else {
            checkBoxImageView.image = UIImage(named: "unchecked_icon")
        }
    }

}

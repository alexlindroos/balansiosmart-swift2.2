//
//  MotivationalNotificationManager.swift
//  BalansioSmartXcode7
//
//  Created by Topi Penttilä on 12/12/16.
//  Copyright © 2016 iosdev. All rights reserved.
//

import Foundation
import UserNotifications

class MotivationalNotificationManager {
    
    static let sharedInstance: MotivationalNotificationManager = MotivationalNotificationManager()
    func fireMotivationalNotificationIfNeeded(dataType: HealthDataType) {
        
        // Check if all entries are in range
        func areInRange(entries: [DataEntry]) -> Bool {
            let clinical = GoalManager.sharedInstance.getGoalForDataType((entries.first?.getDataType())!)?.getClinical()
            for entry in entries {
                if !(entry.getValue() <= clinical?.getMax() && entry.getValue() >= clinical?.getMin()) {
                    return false
                }
                print("in range")
            }
            return true
        }
        
        RealmManager.sharedInstance.getLatestDataEntries(5, dataType: dataType) { (latest) -> () in
            var sentTimestamp = 0.0
            if NSUserDefaults.standardUserDefaults().objectForKey("motivationalSent") != nil {
                sentTimestamp = NSUserDefaults.standardUserDefaults().objectForKey("motivationalSent") as! NSTimeInterval
            }
            
            //Schedule notification if 5 latest are in range & motivational not sent for one day
            if areInRange(latest) && (sentTimestamp + 86400) < NSDate().timeIntervalSince1970 {
                if #available(iOS 10.0, *) {
                    let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 10.0, repeats: false)
                    let notificationContent = UNMutableNotificationContent()
                    notificationContent.title = dataType.rawValue + " looking good"
                    notificationContent.body = "Your " + dataType.rawValue + " values are looking great! Keep up the good work!"
                    notificationContent.categoryIdentifier = dataType.rawValue
                    
                    let request = UNNotificationRequest(
                        identifier: dataType.rawValue,
                        content: notificationContent,
                        trigger: trigger
                    )
                    UNUserNotificationCenter.currentNotificationCenter().addNotificationRequest(request, withCompletionHandler: nil)
                    print("scheduled motivational notification" + String(request))
                    
                    //Save to Realm
                    let notification: Notification = Notification()
                    let dateFire = NSDate().dateByAddingTimeInterval(10)
                    notification.set(request.identifier, dataType: dataType, timestamp: dateFire, notificationType: .Motivational)
                    RealmManager.sharedInstance.saveNotificationToRealm(notification)
                    NSUserDefaults.standardUserDefaults().setDouble(NSDate().timeIntervalSince1970, forKey: "motivationalSent")
                    
                } else {
                    // Fallback on earlier versions
                }
            }
        }
    }
}

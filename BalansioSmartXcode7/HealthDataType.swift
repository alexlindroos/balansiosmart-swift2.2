//
//  HealthDataType.swift
//  BalansioSmartXcode7
//
//  Created by Topi Penttilä on 02/12/16.
//  Copyright © 2016 iosdev. All rights reserved.
//

import Foundation

enum HealthDataType: String {
    case BloodGlucose = "Blood Glucose"
    case Sleep = "Sleep"
    case A1c = "A1c"
    case Activity = "Activity"
    case BloodPressureSystolic = "Blood Pressure Systolic"
    case BloodPressureDiastolic = "Blood Pressure Diastolic"
    case Weight = "Weight"
    case Some = "Some"
    
    static func healthDataTypeFromString(healthDataTypeString: String) -> HealthDataType? {
        switch healthDataTypeString {
        case "Blood Glucose":
            return .BloodGlucose
        case "Sleep":
            return .Sleep
        case "A1c":
            return .A1c
        case "Activity":
            return .Activity
        case "Blood Pressure Systolic":
            return .BloodPressureSystolic
        case "Blood Pressure Diastolic":
            return .BloodPressureDiastolic
        case "Weight":
            return .Weight
        default:
            return nil
        }
    }
    
    var unit: String {
        switch self {
        case .BloodGlucose:
            return "mmol/l"
        case .Sleep:
            return "hour"
        case .A1c:
            return "%"
        case .Activity:
            return "hours"
        case .Weight:
            return "kg"
        default:
            return ""
        }
    }
    
}

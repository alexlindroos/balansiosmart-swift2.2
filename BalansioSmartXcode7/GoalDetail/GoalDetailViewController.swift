//
//  GoalDetailViewController.swift
//  BalansioSmartXcode7
//
//  Created by Hai Phan on 12/5/16.
//  Copyright © 2016 iosdev. All rights reserved.
//

import UIKit
import RealmSwift

class GoalDetailViewController: UIViewController {
    //MARK: IBOutlets
    @IBOutlet weak var disciplineLabel: UILabel!
    @IBOutlet weak var targetLabel: UILabel!
    @IBOutlet weak var notificationLabel: UILabel!
    @IBOutlet weak var measurementTableView: UITableView!
    
    @IBOutlet weak var discHeader: UILabel!
    @IBOutlet weak var targetHeader: UILabel!
    @IBOutlet weak var notificationsHeader: UILabel!
    
    //MARK: Properties
    var goal: Goal!
    private var latestEntries: [DataEntry] = []
    private var latestNotifications: [Notification] = []

    //MARK: View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        measurementTableView.dataSource = self
        measurementTableView.delegate = self
        measurementTableView.tableFooterView = UIView()
        
        discHeader.text = NSLocalizedString("discipline_text", comment: "")
        targetHeader.text = NSLocalizedString("target_text", comment: "")
        notificationsHeader.text = NSLocalizedString("notifications_text", comment: "")
        
        
        bindData()
        
        //Observer
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(GoalDetailViewController.refresh), name: "refreshDetailView", object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    func bindData () {
        //Bind amout, period, min, max and notification
        if let amount = goal.getDiscipline()?.getAmount(), let period = goal.getDiscipline()?.getPeriod() {
            disciplineLabel.text = "\(amount) " + NSLocalizedString("intensity_text2", comment: "") + " \(period)"
        }
        
        if let min = goal.getClinical()?.getMin(), let max = goal.getClinical()?.getMax(), let unit = goal.getClinical()?.getUnit() {
            targetLabel.text = "\(min) - \(max) \(unit)  "
        }
        if let notification = goal.getRule()?.get().rawValue{
            notificationLabel.text = notification
        }
        
        //Get latest data entries from Realm
        RealmManager.sharedInstance.getLatestDataEntries(5, dataType: goal.getDataType()!) { (latestEntries) -> () in
            self.latestEntries = latestEntries
            print("array\(latestEntries)")
        }
        //Get latest notifications from Realm
        RealmManager.sharedInstance.getPastNotificationsByDataType(5, dataType: goal.getDataType()!) { (result) in
            self.latestNotifications = result
        }
    }
    
    func refresh() {
        let realm = try! Realm()
        goal = realm.objects(Goal.self).filter("id = '\(goal.getGoalId())'").first!
        bindData()
    }
    
    @IBAction func didTapMoreButton(sender: UIBarButtonItem) {
        let alertVC = UIAlertController(title: nil, message: nil, preferredStyle: .ActionSheet)
        let editAction = UIAlertAction(title: NSLocalizedString("edit", comment: ""), style: .Default) { (action) in
            self.performSegueWithIdentifier("FromGoalDetailToGoalComposer", sender: nil)
        }
        let deleteAction = UIAlertAction(title: NSLocalizedString("delete", comment: ""), style: .Destructive) { (action) in
            self.deleteGoal()
        }
        let cancelAction = UIAlertAction(title: NSLocalizedString("cancel", comment: ""), style: .Cancel, handler: nil)
        alertVC.addAction(editAction)
        alertVC.addAction(deleteAction)
        alertVC.addAction(cancelAction)
        presentViewController(alertVC, animated: true, completion: nil)
    }
    
    func deleteGoal() {
        let realm = try! Realm()
        try! realm.write {
            realm.delete(self.goal)
        }
        let alertVC = UIAlertController(title: NSLocalizedString("goal_removed", comment: ""), message: nil, preferredStyle: .Alert)
        let dismissAction = UIAlertAction(title: NSLocalizedString("dismiss", comment: ""), style: .Default) { (action) in
            self.navigationController?.popViewControllerAnimated(true)
            NSNotificationCenter.defaultCenter().postNotificationName("refreshProgressView", object: nil)
        }
        alertVC.addAction(dismissAction)
        presentViewController(alertVC, animated: true, completion: nil)
    }
}

extension GoalDetailViewController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return latestEntries.count
        case 1:
            return latestNotifications.count
        default:
            return 0
        }
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let label = UILabel()
        label.backgroundColor = UIColor.whiteColor()
        label.textColor = UIColor.darkGrayColor()
        label.font = UIFont(name: "Avenir", size: 17)
        switch section {
        case 0:
            label.text = NSLocalizedString("lastMeasurements_text", comment: "")
        case 1:
            label.text = NSLocalizedString("lastNotifications_text", comment: "")
        default:
            break
        }
        return label
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCellWithIdentifier("measurementCell", forIndexPath: indexPath) as! GoalDetailTableViewCell
        switch indexPath.section {
        case 0:
            let date = latestEntries[indexPath.row].getTimestamp()!
            let dateFormatter = NSDateFormatter()
            
            dateFormatter.dateFormat = ("dd-MM-yyyy hh:mm")
            dateFormatter.dateStyle = .ShortStyle
            dateFormatter.timeStyle = .ShortStyle
            dateFormatter.doesRelativeDateFormatting = true
            
            let stringDate = dateFormatter.stringFromDate(date)
            
            cell.timeLabel.text = stringDate
            cell.valueLabel.text = String(latestEntries[indexPath.row].getValue()!.roundToPlaces(1)) + String(latestEntries[indexPath.row].getUnit()!)
        case 1:
            let date = latestNotifications[indexPath.row].getDate()!
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateStyle = .ShortStyle
            dateFormatter.timeStyle = .ShortStyle
            dateFormatter.doesRelativeDateFormatting = true
            
            let stringDate = dateFormatter.stringFromDate(date)
            
            cell.timeLabel.text = stringDate
            cell.valueLabel.text = latestNotifications[indexPath.row].getNotificationType().rawValue
        default:
            break
        }
        
    
        return cell
    }
}

extension GoalDetailViewController {
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "FromGoalDetailToGoalComposer" {
            let goalComposerContainerVC = segue.destinationViewController as! GoalComposerContainerViewController
            goalComposerContainerVC.editingGoal = goal
            goalComposerContainerVC.mode = .Edit
        }
    }
}

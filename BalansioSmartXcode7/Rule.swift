//
//  Rule.swift
//  BalansioSmartXcode7
//
//  Created by Topi Penttilä on 01/11/16.
//  Copyright © 2016 iosdev. All rights reserved.
//

import Foundation
import RealmSwift
import HealthKit

enum NotificationSetting: String {
    case Strict = "Strict"
    case Easy = "Easy"
    case None = "None"
}

class Rule: Object {
    
    private dynamic var rawSetting = ""
    internal var setting: NotificationSetting {
        get {
            return NotificationSetting(rawValue: rawSetting)!
        }
        set {
            rawSetting = newValue.rawValue
        }
    }
    
    override static func ignoredProperties() -> [String] {
        return ["setting"]
    }
    
    
    func set(setting: NotificationSetting) -> Rule {
        self.setting = setting
        return self
    }
    
    func get() -> NotificationSetting {
        return self.setting
    }
    
    func save() {
        // Get the default Realm
        let realm = try! Realm()
        // Add to Realm
        try! realm.write {
            realm.add(self)
            print(self)
        }
    }
}

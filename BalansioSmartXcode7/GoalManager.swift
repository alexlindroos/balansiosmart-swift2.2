//
//  GoalManager.swift
//  BalansioSmartXcode7
//
//  Created by Topi Penttilä on 18/11/16.
//  Copyright © 2016 iosdev. All rights reserved.
//

import Foundation
import RealmSwift

class GoalManager {
    
    static let sharedInstance: GoalManager = GoalManager()
    
    func getAllGoals() -> [Goal]? {
        let realm = try! Realm()
        var goals: [Goal] = []
        for goal in realm.objects(Goal) {
            goals.append(goal)
        }
        return goals
    }
    
    func getGoalForDataType(dataType: HealthDataType) -> Goal? {
        let realm = try! Realm()
        for goal in realm.objects(Goal) {
            if goal.getDataType() == dataType {
                return goal
            }
        }
        return nil
    }
    
    func getDisciplineGoalForDataType(dataType: HealthDataType) -> Goal? {
        guard let goals = getAllGoals() else { return nil }
        for goal in goals {
            if goal.getDataType() == dataType {
                return goal
            }
        }
        return nil
    }
    
    func getGoalSettingForDataType(dataType: HealthDataType) -> NotificationSetting? {
        guard let goals = getAllGoals() else { return nil }
        for goal in goals {
            if goal.getDataType() == dataType {
                return goal.getRule()?.get()
            }
        }
        return nil
    }
    
    // Completion block returns the current discipline progress count and the goal amount
    func getGoalProgress(dataEntry: DataEntry, completion: (progress: Int, amount: Int?) -> ()) {
        let amount = dataEntry.getAmountByDataType(dataEntry.getDataType())
        let period = dataEntry.getPeriodByDataType(dataEntry.getDataType())
        var progress = 0
        let calendar = NSCalendar.currentCalendar()
        let now = NSDate()
        
        if period != nil {
            switch period! {
            case .Day:
                RealmManager.sharedInstance.getLatestDataEntries(amount!, dataType: dataEntry.getDataType()) { (latestEntries) -> () in
                    var i = 0
                    for entry in latestEntries {
                        i += 1
                        if calendar.isDate(entry.getTimestamp()!, inSameDayAsDate: now) {
                            progress += 1
                        }
                    }
                    completion(progress: progress, amount: amount)
                }
                break
            case .Month:
                RealmManager.sharedInstance.getLatestDataEntries(amount!, dataType: dataEntry.getDataType()) { (latestEntries) -> () in
                    var i = 0
                    for entry in latestEntries {
                        i += 1
                        if calendar.component(.Month, fromDate: entry.getTimestamp()!) == calendar.component(.Month, fromDate: dataEntry.getTimestamp()!) && calendar.component(.Year, fromDate: entry.getTimestamp()!) == calendar.component(.Year, fromDate: dataEntry.getTimestamp()!) {
                            progress += 1
                        }
                    }
                    completion(progress: progress, amount: amount)
                }
                break
            case.Week:
                RealmManager.sharedInstance.getLatestDataEntries(amount!, dataType: dataEntry.getDataType()) { (latestEntries) -> () in
                    var i = 0
                    for entry in latestEntries {
                        i += 1
                        if calendar.component(.WeekOfYear, fromDate: entry.getTimestamp()!) == calendar.component(.WeekOfYear, fromDate: dataEntry.getTimestamp()!) && calendar.component(.Year, fromDate: entry.getTimestamp()!) == calendar.component(.Year, fromDate: dataEntry.getTimestamp()!) {
                            progress += 1
                        }
                    }
                    completion(progress: progress, amount: amount)
                }
                break
            case.Year:
                RealmManager.sharedInstance.getLatestDataEntries(amount!, dataType: dataEntry.getDataType()) { (latestEntries) -> () in
                    var i = 0
                    for entry in latestEntries {
                        i += 1
                        if calendar.component(.Year, fromDate: entry.getTimestamp()!) == calendar.component(.Year, fromDate: dataEntry.getTimestamp()!) {
                            progress += 1
                        }
                    }
                    completion(progress: progress, amount: amount)
                }
                break
            }
        }
    }
}

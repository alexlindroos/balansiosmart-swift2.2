//
//  SelectInputIntensityViewController.swift
//  BalansioSmartXcode7
//
//  Created by Hai Phan on 11/1/16.
//  Copyright © 2016 iosdev. All rights reserved.
//

import UIKit

class SelectInputIntensityViewController: UIViewController {
    //MARK: IBOutlets
    @IBOutlet weak var instructionLabel: UILabel!
    @IBOutlet weak var optionTextField: UITextField!
    
    //MARK: Properties
    var type: String! //Type should be enum
    let option = ["Day", "Week", "Month", "Year"]
    private var optionPickerView: UIPickerView = UIPickerView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        instructionLabel.text = "How often do you wish to measure your \(type):"
        
        optionPickerView.dataSource = self
        optionPickerView.delegate = self
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension SelectInputIntensityViewController: UITextFieldDelegate {
    @IBAction func optionDidBeginEditing(sender: UITextField) {
        sender.inputView = optionPickerView
    }
}

extension SelectInputIntensityViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return option.count
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return option[row]
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        optionTextField.text = option[row]
    }
}
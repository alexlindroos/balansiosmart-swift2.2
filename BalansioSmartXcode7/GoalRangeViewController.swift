//
//  GoalRangeViewController.swift
//  BalansioSmartXcode7
//
//  Created by iosdev on 28.10.2016.
//  Copyright © 2016 iosdev. All rights reserved.
//

import UIKit
import RealmSwift

enum GoalRangeOption {
    case InRange
    case SingleValue
    
    var desciptionText: String {
        switch self {
        case .InRange:
            return "in range"
        case .SingleValue:
            return "single value"
        }
    }
}

class GoalRangeViewController: UIViewController, UITextFieldDelegate {
    
    //MARK: IBOutlets
    
    @IBOutlet weak var instructionLabel: UILabel!
    @IBOutlet weak var minLabel: UILabel!
    @IBOutlet weak var maxLabel: UILabel!
    @IBOutlet weak var minTextField: UITextField!
    @IBOutlet weak var maxTextField: UITextField!
    @IBOutlet weak var unitLabel1: UILabel!
    @IBOutlet weak var unitLabel2: UILabel!
    @IBOutlet weak var optionTextField: UITextField!
    @IBOutlet weak var continueButton: UIButton!

    //MARK: Properties
    weak var goalComposerContainerVC: GoalComposerContainerViewController?
    
    private let optionDataSource = [GoalRangeOption.InRange, GoalRangeOption.SingleValue]
    private var optionPickerView: UIPickerView = UIPickerView()
    private var option: GoalRangeOption = .InRange
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        goalComposerContainerVC = parentViewController as? GoalComposerContainerViewController
        
        if let type = goalComposerContainerVC?.goal.getDataType() {
            instructionLabel.text = NSLocalizedString("range_text", comment: "") + (type.rawValue.lowercaseString)
            setUnitLabel(type.unit)
        }
        optionPickerView.dataSource = self
        optionPickerView.delegate = self
        
        let dismissKeyboardGesture = UITapGestureRecognizer(target: self, action: #selector(GoalRangeViewController.dismissKeyboard))
        view.addGestureRecognizer(dismissKeyboardGesture)
        
        print(Realm.Configuration.defaultConfiguration.fileURL!)
        
        if goalComposerContainerVC?.mode == .Edit {
            if let min = goalComposerContainerVC?.editingGoal?.getClinical()?.getMin(), let max = goalComposerContainerVC?.editingGoal?.getClinical()?.getMax() {
                if min == max {
                    pickerView(optionPickerView, didSelectRow: 1, inComponent: 0)
                }
                minTextField.text = String(min)
                maxTextField.text = String(max)
            }
        }

        continueButton.setTitle(NSLocalizedString("continue", comment: ""), forState: UIControlState.Normal)

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Actions
    @IBAction func didTapDismiss(sender: UIBarButtonItem) {
        goalComposerContainerVC?.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    @IBAction func optionDidBeginEditing(sender: UITextField) {
        sender.inputView = optionPickerView
    }
    
    func setLayoutForInRangeOption() {
        maxLabel.hidden = false
        minLabel.text = NSLocalizedString("from", comment: "")
        maxLabel.text = NSLocalizedString("to", comment: "")
        unitLabel2.hidden = false
        maxTextField.hidden = false
    }
    
    func setLayoutForSingleValueOption() {
        maxLabel.hidden = true
        minLabel.text = NSLocalizedString("value", comment: "")
        unitLabel2.hidden = true
        maxTextField.hidden = true
    }
    
    func setUnitLabel(text: String) {
        unitLabel1.text = text
        unitLabel2.text = text
    }
    
    func showError() {
        let alertVC = UIAlertController(title: nil, message: NSLocalizedString("alert2", comment: ""), preferredStyle: .Alert)
        let cancelAction = UIAlertAction(title: NSLocalizedString("dismiss", comment: ""), style: .Cancel, handler: nil)
        alertVC.addAction(cancelAction)
        presentViewController(alertVC, animated: true, completion: nil)
        return

    }
    
    @IBAction func didTapContinue(sender: UIButton) {
        switch option {
        case .InRange:
            guard let minValue = minTextField.text, maxValue = maxTextField.text where !minValue.isEmpty && !maxValue.isEmpty && Double(maxValue) > Double(minValue) else {
                showError()
                return
            }
            guard let unit = goalComposerContainerVC?.goal.getDataType()?.unit else {return}
            let clinical = Clinical()
            clinical.set(Double(minValue)!, max: Double(maxValue)!, unit: unit)
            goalComposerContainerVC?.goal.set(nil, discipline: nil, clinical: clinical, rule: nil, timestamp: nil)
        case .SingleValue:
            guard let minValue = minTextField.text where !minValue.isEmpty else {
                showError()
                return
            }
            guard let unit = goalComposerContainerVC?.goal.getDataType()?.unit else {return}
            let clinical = Clinical()
            clinical.set(Double(minValue)!, max: Double(minValue)!, unit: unit)
            goalComposerContainerVC?.goal.set(nil, discipline: nil, clinical: clinical, rule: nil, timestamp: nil)
        }
        performSegueWithIdentifier("FromGoalRangeToNotificationIntensity", sender: nil)
    }
}

//MARK: UIPickerViewDelegate, UIPickerViewDataSource
extension GoalRangeViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return optionDataSource.count
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return optionDataSource[row].desciptionText
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        optionTextField.text = optionDataSource[row].desciptionText
        option = optionDataSource[row]
        switch option {
        case .InRange:
            setLayoutForInRangeOption()
        case .SingleValue:
            setLayoutForSingleValueOption()
        }
    }
}

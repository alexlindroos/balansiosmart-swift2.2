//
//  NotificationManager.swift
//  BalansioSmartXcode7
//
//  Created by Topi Penttilä on 13/12/16.
//  Copyright © 2016 iosdev. All rights reserved.
//

import Foundation
import UserNotifications

class NotificationManager {
    
    static let sharedInstance: NotificationManager = NotificationManager()
    @available(iOS 10.0, *)
    func getUnreactedNotificationsByDataTypeFromDate(fromDate: NSDate, completionHandler: ([UNNotification]) -> Void) {
        var unreactedNotifications: [UNNotification] = []
        UNUserNotificationCenter.currentNotificationCenter().getDeliveredNotificationsWithCompletionHandler() { (notifications) -> () in
            for notification in notifications {
                if fromDate.timeIntervalSinceDate(notification.date).isSignMinus {
                    unreactedNotifications.append(notification)
                    notification.request.content.body
                }
            }
            completionHandler(unreactedNotifications)
        }
    }
}
